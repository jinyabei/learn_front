const mysql = require('mysql2')

const connection = mysql.createConnection({
  host:"localhost",
  port:3306,
  user:"root",
  password:"Pass@950828",
  database:"practice"
})

// connection.query(
//   'select * from customers',
//   function(err,results,fields){
//     console.log(results)
//     console.log(fields.map(item=>item.name));
//   }
// );

// connection.query(
//   'select * from customers where name like ?',
//   ['李%'],
//   function(err,results,fields){
//     console.log(results)
//     console.log(fields.map(item=>item.name));
//   }
// )

// connection.execute(
//   'insert into customers (name) values (?)',
//   ['光'],
//   (err,result,fields)=>{
//     console.log(err);
//   }
// )

// connection.execute(
//   'update  customers set name ="guang" where name = "光"',
//   (err)=>{
//     console.log(err);
//   }
// )


connection.execute(
  'delete from customers where name = ?',
  ['guang'],
  (err)=>{
    console.log(err);
  }
)

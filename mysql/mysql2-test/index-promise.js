const mysql = require('mysql2/promise');

// (async function(){
//   const connection = await mysql.createConnection({
//     host:"localhost",
//     port:3306,
//     user:"root",
//     password:"Pass@950828",
//     database:"practice"
//   })

//   const [results,fields] = await connection.query('select * from customers')
//   console.log(results)
//   console.log(fields.map(item=>item.name));

// })();



(async function(){
  const pool = await mysql.createPool({
    host:"localhost",
    port:3306,
    user:"root",
    password:"Pass@950828",
    database:"practice",
    waitForConnections:true,
    connectionLimit: 10,
    maxIdle:10,
    idleTimeout:60000,
    queueLimit:0,
    enableKeepAlive:true,
    keepAliveInitialDelay:0,
  })

  //自动取连接
  // const [results] = await pool.query('select * from customers')
  // console.log(results)

  const connection = await pool.getConnection();
  const [results] = await connection.query("select * from orders");
  console.log(results)
})();
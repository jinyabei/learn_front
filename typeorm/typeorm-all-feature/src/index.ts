import { In, QueryBuilder } from "typeorm";
import { AppDataSource } from "./data-source"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {

  //单个新增
  // const user = new User()
  // user.firstName = "aaa111"
  // user.lastName = "bbb"
  // user.age = 25
  // await AppDataSource.manager.save(user)


  //单个修改
  // const user = new User()
  // user.id = 1 
  // user.firstName = "aaa111"
  // user.lastName = "bbb"
  // user.age = 25
  // await AppDataSource.manager.save(user)


  //批量新增
  // await AppDataSource.manager.save(User,[
  //   {
  //     firstName:'ccc111',
  //     lastName:'ccc',
  //     age:21
  //   },
  //   {
  //     firstName:'ddd222',
  //     lastName:'ddd',
  //     age:22
  //   },
  //   {
  //     firstName:'eee333',
  //     lastName:'eee',
  //     age:23
  //   }
  // ])


  //批量修改
  // await AppDataSource.manager.save(User,[
  //   {
  //     id:2,
  //     firstName:'ccc111',
  //     lastName:'ccc',
  //     age:21
  //   },
  //   {
  //     id:3,
  //     firstName:'ddd222',
  //     lastName:'ddd',
  //     age:22
  //   },
  //   {
  //     id:4,
  //     firstName:'eee333',
  //     lastName:'eee',
  //     age:23
  //   }
  // ])


  //删除和批量删除
  //await AppDataSource.manager.delete(User,1);
  //await AppDataSource.manager.delete(User,[2,3]);


  //删除数据
  // const user = new User();
  // user.id = 5
  // await AppDataSource.manager.remove(User,user)


  //查询(find)
  // const users = await AppDataSource.manager.find(User);
  // console.log(users)


  //根据条件查询(findBy)
  // const users = await AppDataSource.manager.findBy(User,{
  //   age:23
  // })
  // console.log(users);


  //拿到多少条记录(findAndCount)
  // const [users,count] = await AppDataSource.manager.findAndCount(User)
  // console.log(users,count);


  //掉查询条件的记录
  // const [users,count] = await AppDataSource.manager.findAndCountBy(User,{
  //   age:23
  // })
  // console.log(users,count);


  //查询单条
  // const user = await AppDataSource.manager.findOne(User,{
  //   select :{
  //     firstName:true,
  //     age:true
  //   },
  //   where:{
  //     id:4
  //   },
  //   order:{
  //     age:'ASC'
  //   }
  // })
  // console.log(user);


  //查找多个
  // const user = await AppDataSource.manager.find(User,{
  //   select :{
  //     firstName:true,
  //     age:true
  //   },
  //   where:{
  //     id:In([4,7])
  //   },
  //   order:{
  //     age:'ASC'
  //   }
  // })
  // console.log(user);


  //根据条件查询(findOneBy)
  // const users = await AppDataSource.manager.findOneBy(User,{
  //   age:23
  // })
  // console.log(users);


  // find的特殊用法(findOneOrFail)
  // try{
  //   const user = await AppDataSource.manager.findOneOrFail(User,{
  //     where: {
  //       id: 6
  //     }
  //   });
  //   console.log(user);
  // }catch(e){
  //   console.log(e);
  //   console.log('没找到该用户')
  // }


  //query查询sql
  // const users = await AppDataSource.manager.query('select * from user where age in(?, ?)',[22,23])
  // console.log(users)

  //queryBuilder
  // const queryBuilder = await AppDataSource.manager.createQueryBuilder();
  // const users = await queryBuilder.select("user")
  // .from(User,'user')
  // .where("user.age=:age",{age:22})
  // .getOne()
  // console.log(users);

  //多条关联查询
  await AppDataSource.manager.transaction(async manager=>{
    await manager.save(User,{
      id:4,
      firstName:'eee',
      lastName:'eee',
      age:20,
    })
  })


  // const users = await AppDataSource.manager.find(User)
  // console.log("Loaded users: ", users)

  // console.log("Here you can setup and run express / fastify / any other framework.")

}).catch(error => console.log(error))

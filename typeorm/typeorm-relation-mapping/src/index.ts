import { AppDataSource } from "./data-source"
import { IdCard } from "./entity/IdCard"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {
    
  //多表分别保存
  // const user = new User()
  // user.firstName = "gaung"
  // user.lastName = "gaung"
  // user.age = 20

  // const idCard = new  IdCard();
  // idCard.cardNuame = '111111'
  // idCard.user = user;

  // await AppDataSource.manager.save(user)
  // await AppDataSource.manager.save(idCard)

  //多表联合保存
  // const user = new User()
  // user.firstName = "gaung"
  // user.lastName = "gaung"
  // user.age = 20

  // const idCard = new  IdCard();
  // idCard.cardNuame = '111111'
  // idCard.user = user;

  // await AppDataSource.manager.save(idCard)


  //多表关联查询
  // const ics = await AppDataSource.manager.find(IdCard,{
  //   relations:{
  //     user:true,
  //   }
  // })
  // console.log(ics)


  //关联数据查询
  // const ics = await AppDataSource.manager.getRepository(IdCard)
  // .createQueryBuilder("ic")
  // .leftJoinAndSelect("ic.user","u")
  // .getMany();
  // console.log(ics)

  //关联查询
  // const ics = await AppDataSource.manager.createQueryBuilder(IdCard,"ic")
  // .leftJoinAndSelect("ic.user","u")
  // .getMany()
  // console.log(ics)

  //关联数据修改
  // const user = new User()
  // user.id = 2
  // user.firstName = "gaung1111"
  // user.lastName = "gaung1111"
  // user.age = 20

  // const idCard = new  IdCard();
  // idCard.id = 1
  // idCard.cardNuame = '22222'
  // idCard.user = user;

  // await AppDataSource.manager.save(idCard)

  //关联数据删除
  //await AppDataSource.manager.delete(User,3);

  //非级联下关联删除(先查询整个对象、然后分别删除)
  // const idCard = await AppDataSource.manager.findOne(IdCard,{
  //   where:{
  //     id:1
  //   },
  //   relations:{
  //     user:true
  //   }
  // })

  // await AppDataSource.manager.delete(User,idCard.user.id)
  // await AppDataSource.manager.delete(IdCard,idCard.id)

  //关联查询
  const user = await AppDataSource.manager.find(User,{
    relations:{
      idCard:true
    }
  })
  console.log(user);
}).catch(error => console.log(error))
